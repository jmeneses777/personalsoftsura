/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     03/06/2021 16:33:23                          */
/*==============================================================*/
CREATE DATABASE DB;
drop table if exists db.EXISTENCIA;

drop table if exists db.CONSTRUCCION_MATERIAL;

drop table if exists db.MATERIAL;

drop table if exists db.ORDEN_CONSTRUCCION;

drop table if exists db.SOLICITUD;

drop table if exists db.CONSTRUCCION;

drop table if exists db.ESTADO;

/*==============================================================*/
/* Table: CONSTRUCCION                                          */
/*==============================================================*/
create table db.CONSTRUCCION
(
   ID_CONSTRUCCION      int not null AUTO_INCREMENT ,
   DESCRIPCION          varchar(250) not null,
   TIEMPO               int not null,
   primary key (ID_CONSTRUCCION)
);

/*==============================================================*/
/* Table: CONSTRUCCION_MATERIAL                                 */
/*==============================================================*/
create table db.CONSTRUCCION_MATERIAL
(
   ID_CONSTRUCCION_MATERIAL int not null AUTO_INCREMENT ,
   ID_MATERIAL          int not null,
   ID_CONSTRUCCION      int not null,
   CANTIDAD             int,
   primary key (ID_CONSTRUCCION_MATERIAL)
);

/*==============================================================*/
/* Table: ESTADO                                                */
/*==============================================================*/
create table db.ESTADO
(
   ID_ESTADO            int not null AUTO_INCREMENT ,
   CODIGO               varchar(250),
   DESCRIPCION          varchar(250),
   primary key (ID_ESTADO)
);

/*==============================================================*/
/* Table: EXISTENCIA                                            */
/*==============================================================*/
create table db.EXISTENCIA
(
   ID_EXISTENCIA        int not null AUTO_INCREMENT ,
   ID_MATERIAL          int,
   CANTIDAD             char(10),
   primary key (ID_EXISTENCIA)
);

/*==============================================================*/
/* Table: MATERIAL                                              */
/*==============================================================*/
create table db.MATERIAL
(
   ID_MATERIAL          int not null AUTO_INCREMENT ,
   CODIGO               varchar(250) not null,
   DESCRIPCION          varchar(250) not null,
   primary key (ID_MATERIAL)
);

/*==============================================================*/
/* Table: ORDEN_CONSTRUCCION                                    */
/*==============================================================*/
create table db.ORDEN_CONSTRUCCION
(
   ID_ESTADO            int not null,
   ID_SOLICITUD         int not null,
   ID_ORDEN_CONSTRUCCION int not null AUTO_INCREMENT ,
   FECHA_INICIO         date,
   FECHA_FIN            date,
   primary key (ID_ORDEN_CONSTRUCCION)
);

/*==============================================================*/
/* Table: SOLICITUD                                             */
/*==============================================================*/
create table db.SOLICITUD
(
   ID_SOLICITUD         int not null AUTO_INCREMENT ,
   ID_CONSTRUCCION      int not null,
   X                    int not null,
   Y                    int not null,
   FECHA_SOLICITUD      date,
   primary key (ID_SOLICITUD)
);

alter table db.CONSTRUCCION_MATERIAL add constraint FK_RELATIONSHIP_5 foreign key (ID_MATERIAL)
      references db.MATERIAL (ID_MATERIAL) on delete restrict on update restrict;

alter table db.CONSTRUCCION_MATERIAL add constraint FK_RELATIONSHIP_6 foreign key (ID_CONSTRUCCION)
      references db.CONSTRUCCION (ID_CONSTRUCCION) on delete restrict on update restrict;

alter table db.EXISTENCIA add constraint FK_RELATIONSHIP_4 foreign key (ID_MATERIAL)
      references db.MATERIAL (ID_MATERIAL) on delete restrict on update restrict;

alter table db.ORDEN_CONSTRUCCION add constraint FK_RELATIONSHIP_2 foreign key (ID_ESTADO)
      references db.ESTADO (ID_ESTADO) on delete restrict on update restrict;

alter table db.ORDEN_CONSTRUCCION add constraint FK_RELATIONSHIP_3 foreign key (ID_SOLICITUD)
      references db.SOLICITUD (ID_SOLICITUD) on delete restrict on update restrict;

alter table db.SOLICITUD add constraint FK_RELATIONSHIP_1 foreign key (ID_CONSTRUCCION)
      references db.CONSTRUCCION (ID_CONSTRUCCION) on delete restrict on update restrict;

insert into db.CONSTRUCCION(id_construccion,descripcion, tiempo)values(1,'Casa',3);
insert into db.CONSTRUCCION(id_construccion,descripcion, tiempo)values(2,'Lago',2);
insert into db.CONSTRUCCION(id_construccion,descripcion, tiempo)values(3,'Cancha de fútbol',1);
insert into db.CONSTRUCCION(id_construccion,descripcion, tiempo)values(4,'Edificio',6);
insert into db.CONSTRUCCION(id_construccion,descripcion, tiempo)values(5,'Gimnasio',2);

insert into db.MATERIAL(id_material,codigo, descripcion)values(1,'Cemento','Ce');
insert into db.MATERIAL(id_material,codigo, descripcion)values(2,'Grava','Gr');
insert into db.MATERIAL(id_material,codigo, descripcion)values(3,'Arena','Ar');
insert into db.MATERIAL(id_material,codigo, descripcion)values(4,'Madera','Ma');
insert into db.MATERIAL(id_material,codigo, descripcion)values(5,'Adobe','Ad');


insert into db.CONSTRUCCION_MATERIAL(id_construccion_material,id_construccion, id_material,cantidad)values(1,1,1,100);
insert into db.CONSTRUCCION_MATERIAL(id_construccion_material,id_construccion, id_material,cantidad)values(2,1,2,50);
insert into db.CONSTRUCCION_MATERIAL(id_construccion_material,id_construccion, id_material,cantidad)values(3,1,3,90);
insert into db.CONSTRUCCION_MATERIAL(id_construccion_material,id_construccion, id_material,cantidad)values(4,1,4,20);
insert into db.CONSTRUCCION_MATERIAL(id_construccion_material,id_construccion, id_material,cantidad)values(5,1,5,100);

insert into db.CONSTRUCCION_MATERIAL(id_construccion_material,id_construccion, id_material,cantidad)values(6,2,1,50);
insert into db.CONSTRUCCION_MATERIAL(id_construccion_material,id_construccion, id_material,cantidad)values(7,2,2,60);
insert into db.CONSTRUCCION_MATERIAL(id_construccion_material,id_construccion, id_material,cantidad)values(8,2,3,80);
insert into db.CONSTRUCCION_MATERIAL(id_construccion_material,id_construccion, id_material,cantidad)values(9,2,4,10);
insert into db.CONSTRUCCION_MATERIAL(id_construccion_material,id_construccion, id_material,cantidad)values(10,2,5,20);

insert into db.CONSTRUCCION_MATERIAL(id_construccion_material,id_construccion, id_material,cantidad)values(11,3,1,20);
insert into db.CONSTRUCCION_MATERIAL(id_construccion_material,id_construccion, id_material,cantidad)values(12,3,2,20);
insert into db.CONSTRUCCION_MATERIAL(id_construccion_material,id_construccion, id_material,cantidad)values(13,3,3,20);
insert into db.CONSTRUCCION_MATERIAL(id_construccion_material,id_construccion, id_material,cantidad)values(14,3,4,20);
insert into db.CONSTRUCCION_MATERIAL(id_construccion_material,id_construccion, id_material,cantidad)values(15,3,5,20);

insert into db.CONSTRUCCION_MATERIAL(id_construccion_material,id_construccion, id_material,cantidad)values(16,4,1,200);
insert into db.CONSTRUCCION_MATERIAL(id_construccion_material,id_construccion, id_material,cantidad)values(17,4,2,100);
insert into db.CONSTRUCCION_MATERIAL(id_construccion_material,id_construccion, id_material,cantidad)values(18,4,3,180);
insert into db.CONSTRUCCION_MATERIAL(id_construccion_material,id_construccion, id_material,cantidad)values(19,4,4,40);
insert into db.CONSTRUCCION_MATERIAL(id_construccion_material,id_construccion, id_material,cantidad)values(20,4,5,200);

insert into db.CONSTRUCCION_MATERIAL(id_construccion_material,id_construccion, id_material,cantidad)values(21,5,1,50);
insert into db.CONSTRUCCION_MATERIAL(id_construccion_material,id_construccion, id_material,cantidad)values(22,5,2,25);
insert into db.CONSTRUCCION_MATERIAL(id_construccion_material,id_construccion, id_material,cantidad)values(23,5,3,45);
insert into db.CONSTRUCCION_MATERIAL(id_construccion_material,id_construccion, id_material,cantidad)values(24,5,4,50);
insert into db.CONSTRUCCION_MATERIAL(id_construccion_material,id_construccion, id_material,cantidad)values(25,5,5,50);


insert into db.ESTADO(id_estado,codigo, descripcion)values(1,'pe','pendiente');
insert into db.ESTADO(id_estado,codigo, descripcion)values(2,'pr','programado');
insert into db.ESTADO(id_estado,codigo, descripcion)values(3,'en','en progreso');
insert into db.ESTADO(id_estado,codigo, descripcion)values(4,'fi','finalizado');

insert into db.EXISTENCIA(id_existencia, id_material,cantidad)values(1,1,1000);
insert into db.EXISTENCIA(id_existencia, id_material,cantidad)values(2,2,1000);
insert into db.EXISTENCIA(id_existencia, id_material,cantidad)values(3,3,1000);
insert into db.EXISTENCIA(id_existencia, id_material,cantidad)values(4,4,1000);
insert into db.EXISTENCIA(id_existencia, id_material,cantidad)values(5,5,1000);


